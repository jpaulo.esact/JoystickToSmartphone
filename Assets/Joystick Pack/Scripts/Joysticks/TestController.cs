﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
    protected Joystick joystick;
    protected JoyRightButton joyRightButton;

    // Start is called before the first frame update
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        joyRightButton = FindObjectOfType<JoyRightButton>();
    }

    // Update is called once per frame
    void Update()
    {

        Rigidbody rigidbody = GetComponent<Rigidbody>();

        rigidbody.velocity = new Vector3(joystick.Horizontal, rigidbody.velocity.y, joystick.Vertical);

        if(joyRightButton.Pressed)
        {
            rigidbody.AddForce(Vector3.up * 100);
        }
    }
}
